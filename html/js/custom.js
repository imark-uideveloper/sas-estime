jQuery(document).ready(function() {
setTimeout(function(){
    MagicElement();
}, 300);
  jQuery('.slider-for-projet').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav-projet'
  });
  jQuery('.slider-nav-projet').slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '.slider-for-projet',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
  });
});


function MagicElement() {

  var $el, leftPos, newWidth, elWidth, MWidth;

  jQuery("#collapsibleNavbar .navbar-nav").append("<li id='magic-line'></li>");

  var $magicLine = jQuery("#magic-line");

  ActiveLi = jQuery(".navbar-nav li.active a");

  MWidth = ActiveLi.width();

  $magicLine
    .css("left", ActiveLi.position().left + (MWidth / 2))
    .data("origLeft", $magicLine.position().left)
    .data("origWidth", $magicLine.width());

  jQuery(".navbar-nav li a").hover(function() {
    $el = jQuery(this);
    newWidth = $el.width();
    leftPos = $el.position().left + (newWidth / 2) ;

    $magicLine.stop().animate({
      left: leftPos,
      width: newWidth
    });
  }, function() {
    $magicLine.stop().animate({
      left: $magicLine.data("origLeft"),
      width: $magicLine.data("origWidth")
    });
  });

};

jQuery('.contact-form .form-control, .popup .form-control').focus(function() {
  $(this).parents('.form-group').addClass('focused');
});

jQuery('.contact-form .form-control, .popup .form-control').blur(function() {
  var inputValue = jQuery(this).val();
  if (inputValue == "") {
    jQuery(this).removeClass('filled');
    jQuery(this).parents('.form-group').removeClass('focused');
  } else {
    jQuery(this).addClass('filled');
  }
})


jQuery('.category-section figure').append("<span class='lines top'></span><span class='lines right'></span><span class='lines bottom'></span><span class='lines left'></span>");
jQuery('.category-section figure').attr('pk', '');

jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

// Custom select

var langArray = [];
jQuery('.payment-picker option').each(function() {
  var img = jQuery(this).attr("data-thumbnail");
  var text = this.innerText;
  var value = jQuery(this).val();
  var item = '<li><img src="' + img + '" alt="" value="' + value + '"/><span>' + text + '</span></li>';
  langArray.push(item);
})

jQuery('#paymentListItem').html(langArray);

jQuery('.btn-payment-select').html(langArray[0]);
jQuery('.btn-payment-select').attr('data-value', ' ');

jQuery('#paymentListItem li').click(function() {
  var img = jQuery(this).find('img').attr("src");
  var value = jQuery(this).find('img').attr('value');
  var text = this.innerText;
  var item = '<li><img src="' + img + '" alt="" /><span>' + text + '</span></li>';
  jQuery('.btn-payment-select').html(item);
  jQuery('.btn-payment-select').attr('data-value', value);
  jQuery(".payment-list").toggle();

});

jQuery(".btn-payment-select").click(function() {
  jQuery(".payment-list").toggle();
});


var sessionLang = localStorage.getItem('lang');
if (sessionLang) {

  var langIndex = langArray.indexOf(sessionLang);
  jQuery('.btn-payment-select').html(langArray[langIndex]);
  jQuery('.btn-payment-select').attr('data-value', sessionLang);
} else {
  var langIndex = langArray.indexOf('ch');
  console.log(langIndex);
  jQuery('.btn-payment-select').html(langArray[langIndex]);
}





wow = new WOW({
  mobile: false, // default
})
wow.init();
